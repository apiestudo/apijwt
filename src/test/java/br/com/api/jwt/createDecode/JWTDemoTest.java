package br.com.api.jwt.createDecode;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import br.com.api.jwt.core.JWTConfiguration;
import br.com.api.jwt.core.JWTDemo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

public class JWTDemoTest {

	private static final Logger logger = LogManager.getLogger();

	JWTConfiguration demo = new JWTDemo();

	@Test
	public void test() {

		String jwtId = "SOMEID1234";
		String jwtIssuer = "JWT Demo";
		String jwtSubject = "Andrew";
		int jwtTimeToLive = 800000;

		String jwt = demo.createJWT(jwtId, jwtIssuer, jwtSubject, jwtTimeToLive);

		logger.info("jwt = \"" + jwt.toString() + "\"");

		Claims claims = demo.decodeJWT(jwt);

		logger.info("claims = " + claims.toString());

		assertEquals(jwtId, claims.getId());
		assertEquals(jwtIssuer, claims.getIssuer());
		assertEquals(jwtSubject, claims.getSubject());

	}

	@Test(expected = MalformedJwtException.class)
	public void decodeShouldFail() {

		String notAJwt = "Isso n�o � JWT";

		Claims claims = demo.decodeJWT(notAJwt);

	}

	@Test(expected = SignatureException.class)
	public void createAndDecodeTamperedJWT() {

		String jwtId = "SOMEID1234";
		String jwtIssuer = "JWT Demo";
		String jwtSubject = "Andrew";
		int jwtTimeToLive = 800000;

		String jwt = demo.createJWT(jwtId, jwtIssuer, jwtSubject, jwtTimeToLive);

		logger.info("jwt = \"" + jwt.toString() + "\"");

		StringBuilder tamperedJwt = new StringBuilder(jwt);
		tamperedJwt.setCharAt(22, 'I');

		logger.info("tamperedJwt = \"" + tamperedJwt.toString() + "\"");

		assertNotEquals(jwt, tamperedJwt);

		demo.decodeJWT(tamperedJwt.toString());

	}

}
